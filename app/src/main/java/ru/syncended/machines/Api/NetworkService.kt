package ru.syncended.machines.Api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkService {
    private const val BASE_URL = "http://q11.jvmhost.net/"

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private var INSTANCE: Api? = null
    fun getInstance(): Api = INSTANCE ?: synchronized(this) {
        INSTANCE ?: retrofit.create(Api::class.java).also { INSTANCE = it }
    }
}