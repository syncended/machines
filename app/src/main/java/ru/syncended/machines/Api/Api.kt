package ru.syncended.machines.Api

import retrofit2.http.GET
import ru.syncended.machines.storage.Machine

interface Api {
    @GET("vmc_json")
    suspend fun getData(): List<Machine>
}